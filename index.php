<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>BODS MÍDIAS</title>
    <link rel="stylesheet" href="css/index/index.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MFH8BP8');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFH8BP8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- BOTAO DO WHTSAPP -->
    <a href="https://api.whatsapp.com/send?phone=5511967244487&text=Ol%C3%A1%20!%20Gostaria%20de%20saber%20mais%20sobre%20os%20servi%C3%A7os%20da%20Bods%20M%C3%ADdias" class="float-element" target="_blank">
        <i class="fa-brands fa-whatsapp my-float"></i>
    </a>
    <?php require('contents/index_content.php'); ?>
    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            effect: "coverflow",
            grabCursor: true,
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            breakpoints: {
                765: {slidesPerView: "3"}
            },
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });

        var swiperCellphone = new Swiper(".swiperCellphone", {
            // effect: "coverflow",
            grabCursor: true,
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });
    </script>
    <script>
        const observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    if (entry.target.classList.contains('set-transition-down-up')) {
                        entry.target.classList.add('transition-down-up');
                    } else if (entry.target.classList.contains('set-transition-grow')) {
                        entry.target.classList.add('transition-grow');
                    } else if (entry.target.classList.contains('set-transition-slide-right')) {
                        entry.target.classList.add('transition-slide-right');
                    } else if (entry.target.classList.contains('set-transition-slide-left')) {
                        entry.target.classList.add('transition-slide-left');
                    }
                }
            });
        });
        
        // OBERVERS TRANSITIONS
        observer.observe(document.querySelector('#clientesFalam'));
        observer.observe(document.querySelector('#quemCompoe'));
        observer.observe(document.querySelector('#oQue-BodsMidias'));
        observer.observe(document.querySelector('#textoAtuacao-grow'));
        observer.observe(document.querySelector('#logoBods-grow'));
        observer.observe(document.querySelector('#card-gestao'));
        observer.observe(document.querySelector('#card-email'));
        observer.observe(document.querySelector('#card-growth'));
        observer.observe(document.querySelector('#card-consultoria'));
        observer.observe(document.querySelector('#fotoBods-sildeLeft'));
        observer.observe(document.querySelector('#fotoJacob-sildeRight'));
        observer.observe(document.querySelector('#fotoVitin-sildeLeft'));

    </script>

    <!-- SCRIPT PARA ENVIO DO FORMULARIO -->
    <script type="text/javascript">

        (function() {
            
            var addEvent = function(element, event, func) {
                if (element.addEventListener) {
                    element.addEventListener(event, func);
                } else {
                    var oldFunc = element['on' + event];
                    element['on' + event] = function() {
                        oldFunc.apply(this, arguments);
                        func.apply(this, arguments);
                    };
                }
            }

            var form_to_submit = document.getElementById('webhook-form');

            var jsonDataForm = function() {
                var nameInput    = document.getElementById('name')
                var emailInput   = document.getElementById('email')
                var phoneInput   = document.getElementById('phone')
                var messageInput = document.getElementById('message')

                return {
                    'firstName': nameInput.value,
                    'email'    : emailInput.value,
                    'phone'    : phoneInput.value,
                    'variable1': messageInput.value,
                }
            } 

            var form_submit = async function(e) {
                e.preventDefault();
                document.querySelector('#webhook-form-submit').disabled = true;
                var xhr = new XMLHttpRequest();
                formData = await jsonDataForm();

                var successBanner = document.getElementById("success-banner");

                xhr.open("POST", "https://connect.pabbly.com/workflow/sendwebhookdata/IjQzMTg2MSI_3D", true);
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(JSON.stringify(formData));

                xhr.onreadystatechange=function(){
                    if (xhr.readyState==4 && xhr.status==200){
                        document.querySelector('#webhook-form-submit').disabled = false;
                        successBanner.classList.add('display-block-important')
                        setTimeout(function () {
                            var successBanner = document.getElementById("success-banner");
                            successBanner.classList.remove('display-block-important')
                        }, 8000);
                    }
                }

                return false;
            };
            
            addEvent(form_to_submit, 'submit', form_submit);
        })();

    </script>
</body>

<?php require('default/footer.php'); ?>

</html>