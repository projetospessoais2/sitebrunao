<div class="background-default banner1 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-5">
                    <img src="assets/logo01Fading.webp" class="show-element" style="width: 100%; height: 100%">
                    <img src="assets/logo01.webp" class="hide-element" style="width: 100%; height: 100%">
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 mt-12">
                    <div class="show-element mt-neg-34"></div>
                    <div class="text-center">
                        <img src="assets/logoBods.webp" class="card-img hide-element transition-grow" style="width: 30%; height: 100%" id="logoBods-grow">
                    </div>
                    <div class="text-center text-color-white mt-3 neon-behavior-white-intense text-weight-bolder f-size-2-4 lh-1-2 transition-down-up">
                        Potencialize suas VENDAS online
                    </div>
                    <div class="text-color-white text-center f-size-0-9 mt-5 transition-down-up">
                        <div class="show-element mt-neg-5"></div>
                        O que você faria se conseguisse dobrar o seu faturamento em trinta dias, sem necessariamente dobrar o seu investimento em Marketing? E se você conseguisse aumentar o seu Ticket Médio, ou escalar a quantidade de produtos vendidos? Nós da <span class="text-weight-bolder">BODS MÍDIAS</span> podemos te ajudar! 
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-4-5 show-element"></div>
        <div class="col-md-1"></div>
    </div>
    <!-- <br><br><br><br><br>
    <br><br><br><br><br>
    <br><br><br><br><br>
    <br><br><br><br><br> -->
</div>
<div class="background-default h-100 container-fluid">
    <div class="show-element mt-neg-5"></div>
    <div class="mt-5 text-center text-black f-size-3-2">
        O QUE É A <span class="text-weight-bolder">BODS MÍDIAS?</span>
    </div>
    <div class="row mt-4 text-center text-black f-size-1-3 text-weight-medium">
        <div class="col-md-1"></div>
        <div class="col-md-10 set-transition-down-up" id="oQue-BodsMidias">
            <p>
            A <span class="text-weight-bolder">BODS MÍDIAS</span> é uma agência de <i>Marketing Digital</i> focada no crescimento estratégico (mais conhecido como Growth Marketing) de E-commerces de nichos variáveis através de ferramentas digitais. 
            </p>
            <p class="mt-4">
            O nosso carro chefe é a Gestão Estratégica de Tráfego Pago, nas mais variáveis mídias e redes sociais, tais como Instagram, WhatsApp, Facebook, Google, YouTube, TikTok, Pinterest, Taboola etc.
            </p>
            <p class="mt-5">
            Com mais de 50 clientes atendidos e alguns milhares de reais administrados e investidos em anúncios para as empresas, a <span class="text-weight-bolder">BODS MÍDIAS</span> já fez seus clientes faturarem uma média de 3 milhões nos últimos 2 anos.
            </p>
        </div>
        <div class="col-md-1"></div>
    </div>
    <br><br>
</div>

<div class="background-default banner2 h-100 container-fluid">
    <div class="show-element mt-neg-15"></div>

    <!-- SVG PATH 1 -->
    <div class="elementor-shape elementor-shape-top" data-negative="false">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" class="svg-1">
            <path class="elementor-shape-fill" d="M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9"></path>
        </svg>		
    </div>

    <div class="row">
        <div class="col-md-5 mt-16">
            <img src="assets/textoAtuacao.webp" class="card-img atuacao-resp set-transition-grow sizes-textoAtuacao" id="textoAtuacao-grow">
        </div>

        <div class="col-md-7">
            <div class="row">

                <div class="col-md-6 mt-12">
                    <div class="show-element mt-neg-15"></div>
                    <div class="card card-w-background banner1 text-center set-transition-slide-right" id="card-gestao">
                        <div class="card text-color-white border-white transparent-background">
                            <img src="assets/iconAlert.webp" class="img-card size-iconAlert" alt="...">
                            <div class="card-body mt-4-5">
                                <h5>Gestão de Tráfego</h5>
                                <hr>
                                <p class="f-size-0-8">
                                Por meio de anúncios online, o seu negócio pode aparecer para as pessoas certas, pro seu público alvo consumidor. Afinal, não adianta só alcançar mais pessoas se elas não forem compradoras, né?
                                </p>
                            </div>
                        </div>
                        <div class="mt-2"></div>
                    </div>

                    <div class="card card-w-background banner2 text-center mt-5 set-transition-slide-left" id="card-growth">
                        <div class="card text-color-white border-white transparent-background">
                            <img src="assets/iconGrowth.webp" class="img-card size-1" alt="...">
                            <div class="card-body mt-4-5">
                                <h5>Growth Marketing</h5>
                                <hr>
                                <p class="f-size-0-8">
                                Ajudamos o seu e-commerce focando no rápido crescimento dele. Sabia quem nem sempre você precisa investir mais para atingir mais pessoas e ser mais conhecido(a)? As vezes simples ajustes podem te levar a outro patamar, sem gastar 1 real a mais.
                                </p>
                            </div>
                        </div>
                        <div class="mt-2"></div>
                    </div>

                </div>
                
                <!-- <div class="col-md-1"></div> -->
                <div class="col-md-6 mt-12">
                    <div class="show-element mt-neg-20"></div>

                    <div class="card card-w-background banner1 text-center set-transition-slide-right" id="card-email">
                        <div class="card text-color-white border-white transparent-background mt-2">
                            <img src="assets/iconMensagem.webp" class="img-card size-1" alt="...">
                            <div class="card-body mt-4">
                                <h5>E-mail Marketing</h5>
                                <hr>
                                <p class="f-size-0-8">
                                O EMAIL AINDA NÃO MORREU! Para escalarmos as vendas, todos os canais possíveis precisam ser ativados. O e-mail marketing é uma excelente ferramenta que lembra o seu cliente (ou futuro cliente) da sua marca com novidades, promoções, dicas e afins.
                                </p>
                            </div>
                        </div>
                        <div class="mt-2"></div>
                    </div>

                    <div class="card card-w-background banner3 text-center mt-4-5 set-transition-slide-left" id="card-consultoria">
                        <div class="card text-color-white border-white transparent-background">
                            <img src="assets/iconEmpresarial.webp" class="img-card size-1" alt="...">
                            <div class="card-body mt-4-5">
                                <h5>Consultoria Estratégica Individual</h5>
                                <hr>
                                <p class="f-size-0-8">
                                Pra você que já tem uma equipe interna de Tráfego, Growth e/ou e-mail marketing e quer escalar o seu e-commerce com insights da <span class="text-weight-bolder">BODS MÍDIAS</span>, nós disponibilizamos uma consultoria individual (via video chamada) com o nosso CEO e estrategista Bruno Bods, onde, juntamente com a equipe do seu e-commerce, construirão um planejamento estratégico + growth pra você.
                                </p>
                            </div>
                        </div>
                        <div class="mt-2"></div>
                    </div>

                </div>
            </div>
        </div>
        <!-- <div class="col-md-1"></div> -->
        <!-- <div class="col-md-1"></div>
        <div class="col-md-10"></div>
        <div class="col-md-1"></div> -->
    </div>
    <br><br>
</div>

<div class="background-default h-100 container-fluid">

    <!-- SVG PATH 2 -->
    <div class="elementor-shape elementor-shape-top" data-negative="false">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" class="svg-2">
            <path class="elementor-shape-fill path-2" d="M500,98.9L0,6.1V0h1000v6.1L500,98.9z"></path>
        </svg>		
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5" id="divClassButtonInitial">
            <div class="mt-8 text-black f-size-3 lh-1-2">
                Quem está à frente da <div class="text-weight-bolder">BODS MÍDIAS?</div>
            </div>
            <img src="assets/fotoBods.webp" class="card-img show-element">
            <div class="f-size-1-1 mt-4">
                <p>
                <span class="text-weight-bolder">Bruno Souza</span>, formado em Administração pela UFES (Universidade Federal do Espírito Santo) e Pós-Graduado em Marketing na FGV (Fundação Getúlio Vargas) é o idealizador do projeto <span class="text-weight-bolder">BODS MÍDIAS</span> e quem está à frente do projeto desde o seu início.
                </p>
                <p class="mt-4">
                Depois de criar sua própria metodologia de anúncios em 2019 chamada de <span class="text-weight-bolder">Funil Duplo</span>, Bruno conta com mais de 200 alunos no seu curso online, além de dar mentorias e consultorias particulares pra algumas marcas de ecommerces de moda no Brasil.
                </p>
                <p class="mt-4">
                Já idealizou mais de 40 lançamentos de infoprodutos (Cursos Online) como coprodutor em variados nichos, utilizando de várias estratégias de lançamentos diferentes.
                </p>
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3 mt-9 hide-element">
            <img src="assets/fotoBods.webp" class="card-img">
        </div>
        <div class="col-md-2"></div>
    </div>
    <br><br>
</div>


<div class="background-default banner3 h-100 container-fluid">
    <div class="show-element mt-neg-10"></div>

    <!-- SVG PATH 2 -->
    <div class="elementor-shape" data-negative="false">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none" class="svg-3">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"></path>
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
        </svg>
    </div>

    <div class="set-transition-down-up" id="clientesFalam">
        <div class="text-center neon-behavior-white-intense text-color-white f-size-2-7 lh-1-2">
            <br><br>
            O que os clientes falam <br> sobre a <span class="text-weight-bolder">BODS MÍDIAS?</span>
        </div>
    
        <section class="mt-5">  
            <div class="swiper mySwiper">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <img src="assets/carousel/COM-MARIA_00000.webp" />
                </div>
                <div class="swiper-slide">
                  <img src="assets/carousel/Leticia_00000.webp" />
                </div>
                <div class="swiper-slide">
                  <img src="assets/carousel/Melina_00000.webp" />
                </div>
                <div class="swiper-slide">
                  <img src="assets/carousel/Mila_00000.webp" />
                </div>
                <div class="swiper-slide">
                  <img src="assets/carousel/Poliana_00000.webp" />
                </div>
                <div class="swiper-slide">
                  <img src="assets/carousel/Rayssa_00000.webp" />
                </div>
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
              <div class="swiper-pagination"></div>
            </div>
        </section>
    </div>


    <div class="text-center text-color-white f-size-2-4 set-transition-slide-left" id="quemCompoe">
        <br><br>
        Quem compõe a equipe da <span class="text-weight-bolder">BODS MÍDIAS?</span>
    </div>
    <div class="row hide-element">
        <div class="col-md-2"></div>
        <div class="col-md-3 mt-3 set-transition-slide-left" id="fotoBods-sildeLeft">
            <img src="assets/bods.webp" class="card-img transform-image" alt="...">
        </div>
        <div class="col-md-3 mt-3">
            <img src="assets/xandin.webp" class="card-img set-transition-slide-right" id="fotoJacob-sildeRight">
        </div>
        <div class="col-md-3 mt-3">
            <img src="assets/vitin.webp" class="card-img set-transition-slide-right" id="fotoVitin-sildeLeft">
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row show-element">

        <section class="mt-5">  
            <div class="swiper swiperCellphone">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <img src="assets/bods.webp" />
                </div>
                <div class="swiper-slide">
                  <img src="assets/xandin.webp"/>
                </div>
                <div class="swiper-slide">
                  <img src="assets/vitin.webp"/>
                </div>
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
              <div class="swiper-pagination"></div>
            </div>
        </section>

    </div>
    <div class="row mt-5">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-weight-bold f-size-1-7 text-center text-color-white">
            Preencha o formulário abaixo para entrar em contato 👇
            <div class="mt-4">
                <div class="alert-success-box f-size-0-6" id="success-banner">
                    Show de Bola! Recebemos o seu contato. <br> Em breve entramos em contato com você! 😃
                </div>
                <form action="" method="POST" id="webhook-form">
                    <input type="text" class="form-control inputs-bods" placeholder="NOME" name="name" id="name" required>
                    <input type="text" class="form-control inputs-bods mt-2" placeholder="E-MAIL" name="email" id="email" required>
                    <input type="text" class="form-control inputs-bods mt-2" placeholder="DDD (sem o zero) + CELULAR" name="phone" id="phone" required>
                    <textarea class="form-control inputs-bods mt-2" rows="4" placeholder="MENSAGEM" name="message" id="message" required></textarea>
                    <div class="text-center">
                        <button class="btn btn-secondary mt-2 col-8 f-size-0-8 text-weight-bolder" type="submit" id="webhook-form-submit">ENVIAR</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <br><br>
</div>